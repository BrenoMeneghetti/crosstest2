import { CrossTestPage } from './app.po';

describe('cross-test App', function() {
  let page: CrossTestPage;

  beforeEach(() => {
    page = new CrossTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
