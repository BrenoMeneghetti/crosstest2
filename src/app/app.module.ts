import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MatchComponent } from './match/match.component';
import { ResultComponent } from './result/result.component';
import { GridComponent } from './grid/grid.component';
import { MatchSetupComponent } from './match-setup/match-setup.component';
import { GridSquareComponent } from './grid-square/grid-square.component';
import { MessageCenterComponent } from './message-center/message-center.component';

import { MatchService } from './match.service';
import { TurnControlComponent } from './turn-control/turn-control.component';

@NgModule({
  declarations: [
    AppComponent,
    MatchComponent,
    ResultComponent,
    GridComponent,
    MatchSetupComponent,
    GridSquareComponent,
    MessageCenterComponent,
    TurnControlComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MatchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
