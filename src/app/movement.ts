export class Movement {
    playerNumber: number;
    position:number;

    constructor(playerNumber,position:number) {
        this.playerNumber = playerNumber;
        this.position = position;
    }
}
