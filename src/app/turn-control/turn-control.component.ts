import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Player } from '../player';
import { MatchService } from '../match.service';

@Component({
  selector: 'app-turn-control',
  templateUrl: './turn-control.component.html',
  styleUrls: ['./turn-control.component.css']
})
export class TurnControlComponent implements OnInit {
  matchService: MatchService;
  @Input() player: Player;
  @Output() afterInitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  yourTurn: boolean = false;

  constructor(matchService: MatchService) { 
    this.matchService = matchService;
  }

  ngOnInit() {  
    let subscription = this.matchService.getTurnObservable().subscribe(
      value => value == this.player.position ? this.yourTurn = true : this.yourTurn = false
    );
    this.afterInitEvent.emit(true);
  }



}
