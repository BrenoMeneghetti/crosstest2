import { Movement } from './movement';
import { Player } from './player';

export class TicTacToeMatch {
    player1: Player;
    player2: Player;
    turnOf: number;
    matchId: number;
    result: string;
    movements: Array<Movement>


    constructor(namePlayer1: string, imgPlayer1: string, namePlayer2: string, imgPlayer2: string){
        let imagePlayer1 = imgPlayer1 ? imgPlayer1 : "http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons/simple-red-glossy-icons-alphanumeric/074498-simple-red-glossy-icon-alphanumeric-x-solid.png";
        this.player1 = new Player(namePlayer1, imagePlayer1, 1);
        let imagePlayer2 = imgPlayer2 ? imgPlayer2 : "http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons/simple-red-glossy-icons-alphanumeric/074479-simple-red-glossy-icon-alphanumeric-plus-sign-clear.png";
        this.player2 = new Player(namePlayer2, imagePlayer2, 2);
        this.turnOf = Math.random() % 2 == 0 ? 2 : 1;
        this.matchId = Math.random();
        this.movements = new Array<Movement>();
    }

    positionFilled(position: number){
        for(let i = 0; i < this.movements.length; i++) {
            if(this.movements[i].position == position)
                return true;
        }
        return false;
    }

    addMovement(position: number){
        let movement = new Movement(this.turnOf,Number(position));
        this.movements[this.movements.length] = movement;

        let img = this.turnOf == 1 ? this.player1.imageUrl : this.player2.imageUrl;
        this.turnOf = this.turnOf == 1 ? 2 : 1; 
        return img;
    }

    checkWinner(){
        let positionsToCheck = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]];
        for(let i = 0; i < positionsToCheck.length; i++) {
            let selectedPositions = positionsToCheck[i];
            let selected = this.movements.filter(move=>selectedPositions.indexOf(move.position) !== -1);
            let sum = 0;
            selected.forEach(item=> sum = sum + item.playerNumber);
            if(selected.length == 3 && (sum == 3 || sum == 6)) {
                this.result = "Player "+selected[1].playerNumber+" won";
                return true;
            }
        }
        if(this.movements.length == 9) {
            this.result = "Draw!";
            return true;
        }

        return false;
    }

    resetMatch(){
        this.movements = new Array<Movement>();
        let aleatorio = Math.floor(Math.random()*100); 
 console.log("aleatorio ", aleatorio);
        this.turnOf = aleatorio % 2 === 0 ? 2 : 1;
        this.result = "";
        console.log("this.turnOf ", this.turnOf);

    }

}
